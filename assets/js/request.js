/**
 * Envoi une requête avec Fetch vers le Controller PHP
 *
 * @param {string} query
 * @param type
 * @param body
 * @return {Promise<{data: null, status: boolean}>}
 */
async function request(query, type = 'get', body = {}) {
    const output = {status: false, data: null};

    if (type === 'get') {
        try {
            const response = await fetch(`./controllers/controller.php?q=${query}`);
            output.data = await response.json();

            if (response.ok) {
                if (!output.data.hasOwnProperty('error')) output.status = true;
                else output.data = output.data.error;
            }
        } catch (error) {
            output.data = error;
        }
    } else if (type === 'post') {
        const formData  = new FormData();
        if (body?.form) {
            for (const key in body.form) {
                if (body.form.hasOwnProperty(key)) formData.append(key, body.form[key]);
            }
        }

        try {
            const response = await fetch(`./controllers/controller.php?q=${query}`, {
                method: 'POST',
                body: formData
            });
            output.data = await response.json();

            if (response.ok && !response.redirected) {
                if (!output.data?.error) output.status = true;
                else output.data = output.data.error;
            }
        } catch (error) {
            output.data = error;
        }
    } else if (type === 'delete') {
        try {
            const response = await fetch(`./controllers/controller.php?q=${query}`, {
                method: 'DELETE'
            });
            output.data = await response.text();

            if (response.ok) {
                if (!output.data.hasOwnProperty('error')) output.status = true;
                else output.data = output.data.error;
            }
        } catch (error) {
            output.data = error;
        }
    }
    return output;
}

export {request};