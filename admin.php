<?php
    if (!isset($_SESSION)) session_start();
    if(empty($_SESSION) || !isset($_SESSION['loggedIn'])):
        header('Location: index.html');
        exit;
    endif;
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Biokop - Admin</title>

    <!-- Css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" media="screen" href="assets/css/mq.css">
</head>
<body>
    <div id="topBar"></div>

    <nav>
        <ul>
            <li><a href="index.html">Accueil</a></li>
            <li><a id="addElementBtn">Ajouter</a></li>
            <li><a href="logout.php">Déconnexion</a></li>
        </ul>
    </nav>
    <main id="admin">
        <div id="container">
            <div id="selectionMonths" data-show="false"></div>
            <section></section>
        </div>
    </main>

    <script src="assets/js/admin.js" type="module"></script>
</body>
</html>