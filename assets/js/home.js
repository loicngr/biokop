import {getElements, getMonths} from "./api.js";

const calendar = {};
const uploadsImageFolder = "./uploads/";

const DATE = new Date();
let currentMonth;

/**
 * Changement de Mois
 *
 * @param {number} turnDirection
 */
function turnMonth(turnDirection = 1) {
    const calendarLength = Object.keys(calendar).length;

    if (!currentMonth) currentMonth = Object.keys(calendar)[DATE.getMonth()];
    let calendarCurrentIndex =  Object.keys(calendar).indexOf(currentMonth);

    // Si on clique sur la flèche de droite
    if (turnDirection === 1) {
        if (calendarCurrentIndex + 1 >= calendarLength) calendarCurrentIndex = 0;
        else calendarCurrentIndex += 1;
    }
    // Sinon, si on clique sur la flèche de gauche
    else if (turnDirection === -1) {
        if (calendarCurrentIndex - 1 < 0) calendarCurrentIndex = calendarLength - 1;
        else calendarCurrentIndex -= 1;
    }
    currentMonth = Object.keys(calendar)[calendarCurrentIndex];

    const sectionElement = document.getElementById("elements");
    const titleMonth = document.querySelector("h1.title");
    sectionElement.innerHTML = "";
    titleMonth.innerText = currentMonth;

    // On boucle sur tous les éléments dans le mois en cours
    calendar[currentMonth].forEach((element) => {
        const articleElement = document.createElement("article");
        const imgElement = document.createElement("img");
        const h3Element = document.createElement("h3");

        articleElement.appendChild(imgElement);
        articleElement.appendChild(h3Element);

        h3Element.innerText = element.nom;
        imgElement.src = `${uploadsImageFolder}${element.image}`;

        sectionElement.appendChild(articleElement);
    });
}

/**
 * On rempli notre calendrier avec les fruits et légumes
 *
 * @param {{data: null|[{element_nom: string, mois_nom: string, element_image: string}], status: boolean}} elements
 */
function fillCalendar(elements) {
    if (elements?.status && elements?.data) {
        elements.data.forEach((element) => {
            calendar[element.mois_nom].push({
                nom: element.element_nom, image: element.element_image
            });
        });

        turnMonth(0);
    }
}


/**
 * Création des clés dans l'objet Calendar pour tous les mois enregistrés
 *
 * @param {{data: null|[{mois_nom: string}], status: boolean}} elements
 *
 * @exemple
 *  calendar: {
 *      janvier: []
 *  }
 */
function buildCalendar(elements) {
    if (elements?.status && elements?.data) {
        elements.data.forEach((element) => {
            if (!calendar[element.mois_nom]) calendar[element.mois_nom] = [];
        });

        getElements().then(r => fillCalendar(r));
    }
}
getMonths().then(r => buildCalendar(r));


document.getElementById("leftArrow").addEventListener("click", (evt) => turnMonth(-1));
document.getElementById("rightArrow").addEventListener("click", (evt) => turnMonth(1));