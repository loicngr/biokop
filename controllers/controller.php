<?php
if (!defined("ROOT")) {
    define("ROOT", "../");
}
session_start();

/**
 * Controller Class
 *
 * opérations CRUD
 */

class Controller
{
    private $bdd;

    function __construct()
    {
        require ROOT . "models/bdd.php";

        $this->bdd = new Bdd();
    }

    private function userIsLoggedIn()
    {
        return isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] == true;
    }

    private function getMonths()
    {
        if (isset($_GET['id'])):
            $month_id = intval(htmlspecialchars(strip_tags($_GET['id'])));
            return $this->bdd->getMonthByID($month_id);
        endif;

        return $this->bdd->getMonths();
    }

    private function getElement()
    {
        if (isset($_GET['month_id'])):
            $month_id = intval(htmlspecialchars(strip_tags($_GET['month_id'])));
            return $this->bdd->getElementByMonth($month_id);
        elseif (isset($_GET['name'])):
            $element_name = htmlspecialchars(strip_tags($_GET['name']));
            return $this->bdd->getElementByName($element_name);
        elseif (isset($_GET['id'])):
            $element_id = intval(htmlspecialchars(strip_tags($_GET['id'])));
            return $this->bdd->getElementMonths($element_id);
        endif;

        return json_encode([
            "error" => "Route non valide"
        ]);
    }

    private function getElements()
    {
        return $this->bdd->getElements();
    }

    private function getElementsMonths()
    {
        return $this->bdd->getElementsMonths();
    }

    private function addElement()
    {
        if ($this->userIsLoggedIn()):
            $name = htmlspecialchars(strip_tags($_POST['nom']));
            $image = htmlspecialchars(strip_tags($_POST['image']));
            $months = htmlspecialchars(strip_tags($_POST['mois']));

            return $this->bdd->postElement([
                "name" => $name,
                "image" => $image,
                "months" => $months
            ]);
        endif;

        return json_encode([
            "error" => "Route non valide"
        ]);
    }

    private function updateElementName()
    {
        if ($this->userIsLoggedIn()):
            $name = htmlspecialchars(strip_tags($_POST['nom']));
            $id = intval(htmlspecialchars(strip_tags($_POST['id'])));

            return $this->bdd->updateElementName([
                "id" => $id,
                "name" => $name
            ]);
        endif;

        return json_encode([
            "error" => "Route non valide"
        ]);
    }

    private function updateElementMonths()
    {
        if ($this->userIsLoggedIn()):
            $id = intval(htmlspecialchars(strip_tags($_POST['id'])));
            $months = htmlspecialchars(strip_tags($_POST['mois']));

            return $this->bdd->updateElementMonths([
                "id" => $id,
                "months" => $months
            ]);
        endif;

        return json_encode([
            "error" => "Route non valide"
        ]);
    }

    private function updateElementImage()
    {
        if ($this->userIsLoggedIn()):
            $fileContent = file_get_contents($_FILES["file"]["tmp_name"]);
            $fileName = $_FILES['file']['name'];
            $id = intval(htmlspecialchars(strip_tags($_POST['id'])));

            $path = explode('/', $_SERVER['DOCUMENT_ROOT']);
            $path = implode('/', $path);
            file_put_contents("$path/uploads/$fileName", $fileContent);

            return $this->bdd->updateElementImage([
                "id" => $id,
                "name" => $fileName
            ]);
        endif;

        return json_encode([
            "error" => "Route non valide"
        ]);
    }

    private function uploadImage()
    {
        if ($this->userIsLoggedIn()):
            $fileContent = file_get_contents($_FILES["file"]["tmp_name"]);
            $fileName = $_FILES['file']['name'];

            $path = explode('/', $_SERVER['DOCUMENT_ROOT']);
            $path = implode('/', $path);
            return file_put_contents("$path/uploads/$fileName", $fileContent);
        endif;

        return json_encode([
            "error" => "Route non valide"
        ]);
    }

    private function deleteElement()
    {
        if ($this->userIsLoggedIn() && isset($_GET['id'])):
            $element_id = intval(htmlspecialchars(strip_tags($_GET['id'])));
            return $this->bdd->deleteElementById($element_id);
        endif;

        return json_encode([
            "error" => "Route non valide"
        ]);
    }

    private function isLoggedIn()
    {
        return json_encode((isset($_SESSION['loggedIn'])? $_SESSION['loggedIn']:false));
    }

    private function connectUser()
    {
        if (!empty($_POST) && isset($_POST['email']) && isset($_POST['password'])):
            $email = htmlspecialchars(strip_tags($_POST['email']));
            $password = htmlspecialchars(strip_tags($_POST['password']));

            if ($email && $password):
                return $this->bdd->connectUser([
                    "email" => $email,
                    "password" => $password
                ]);
            else:
                return  json_encode([
                    "error" => "Route non valide"
                ]);
            endif;
        endif;

        return  json_encode([
            "error" => "Route non valide"
        ]);
    }

    /**
     * Routeur de mon application
     *
     * @param $query
     */
    public function router($query)
    {
        switch ($query) {
            case 'getMonths':
                echo $this->getMonths();
                break;
            case 'getElement':
                echo $this->getElement();
                break;
            case 'getElements':
                echo $this->getElements();
                break;
            case 'getElementsMonths':
                echo $this->getElementsMonths();
                break;
            case 'addElement':
                echo $this->addElement();
                break;
            case 'updateElementName':
                echo $this->updateElementName();
                break;
            case 'updateElementMonths':
                echo $this->updateElementMonths();
                break;
            case 'updateElementImage':
                echo $this->updateElementImage();
                break;
            case 'uploadImage':
                echo $this->uploadImage();
                break;
            case 'deleteElement':
                echo $this->deleteElement();
                break;
            case 'isLoggedIn':
                echo $this->isLoggedIn();
                break;
            case 'connectUser':
                echo $this->connectUser();
                break;
            default:
                echo json_encode([
                    "error" => "Route non valide"
                ]);
                return;
        }
    }
}

/**
 * Routeur
 *
 * Si le Tableau Get n'est pas vide et le tableau post est vide et le tableau delete est vide
 * -- ou --
 * Si le Tableau Get n'est pas vide et le tableau post n'est pas vide et le tableau delete est vide est
 *      l'utilisateur est connecté.
 * -- ou --
 * Si le Tableau Get n'est pas vide et le tableau post est vide et le tableau delete n'est pas vide est
 *      l'utilisateur est connecté.
 */
$ctrl = new Controller();
if (!empty($_GET) && empty($_POST) && empty($_DELETE) ||
    !empty($_GET) && !empty($_POST) && empty($_DELETE) ||
    !empty($_GET) && empty($_POST) && !empty($_DELETE)):
    if (isset($_GET['q'])):
        $query = htmlspecialchars(strip_tags($_GET['q']));

        if (isset($query) && !empty($query)):
            $ctrl->router($query);
        else:
            header("Location: /");
        endif;
    else:
        header("Location: /");
    endif;
else:
    header("Location: /");
endif;