-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 26 Juin 2020 à 12:46
-- Version du serveur :  5.7.30-0ubuntu0.18.04.1
-- Version de PHP :  7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `biokop`
--

-- --------------------------------------------------------

--
-- Structure de la table `elements`
--

CREATE TABLE `elements` (
  `element_id` tinyint(3) UNSIGNED NOT NULL,
  `element_nom` varchar(30) NOT NULL,
  `element_image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `elements`
--

INSERT INTO `elements` (`element_id`, `element_nom`, `element_image`) VALUES
(47, 'Abricot', 'apricots-1522680_1280.jpg'),
(48, 'Artichaut', 'artichoke-3386681_1280.jpg'),
(49, 'Asperge', 'asparagus-2178164_1280.jpg'),
(50, 'Topinambour', 'jerusalem-artichoke-2192048_1280.jpg'),
(51, 'Shiitake', 'shiitake-7002_1280.jpg'),
(52, 'Pomme de terre', 'potatoes-1585060_1280.jpg'),
(53, 'Pomme', 'apple-1702316_1280.jpg'),
(54, 'Kiwi', 'Kiwi.jpg'),
(55, 'Framboise', 'heart-1503998_1280.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `intermediaire`
--

CREATE TABLE `intermediaire` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `id_element` tinyint(3) UNSIGNED NOT NULL,
  `id_mois` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `intermediaire`
--

INSERT INTO `intermediaire` (`id`, `id_element`, `id_mois`) VALUES
(51, 47, 6),
(52, 47, 7),
(53, 47, 8),
(54, 48, 3),
(55, 48, 4),
(56, 48, 5),
(57, 48, 6),
(58, 48, 7),
(59, 48, 9),
(60, 49, 3),
(61, 49, 4),
(62, 49, 5),
(63, 49, 6),
(64, 50, 1),
(65, 50, 2),
(66, 50, 3),
(67, 50, 11),
(68, 50, 12),
(69, 51, 5),
(70, 51, 6),
(71, 51, 7),
(72, 51, 8),
(73, 51, 9),
(74, 51, 10),
(75, 51, 11),
(76, 51, 12),
(77, 52, 1),
(78, 52, 2),
(79, 52, 3),
(80, 52, 4),
(81, 52, 5),
(82, 52, 6),
(83, 52, 7),
(84, 52, 8),
(85, 52, 9),
(86, 52, 10),
(87, 52, 11),
(88, 52, 12),
(89, 53, 5),
(90, 53, 6),
(91, 53, 7),
(92, 53, 8),
(93, 53, 9),
(94, 53, 10),
(95, 53, 11),
(96, 54, 1),
(97, 54, 2),
(98, 54, 3),
(99, 54, 4),
(100, 54, 9),
(101, 55, 6),
(102, 55, 7),
(103, 55, 8);

-- --------------------------------------------------------

--
-- Structure de la table `mois`
--

CREATE TABLE `mois` (
  `mois_id` tinyint(3) UNSIGNED NOT NULL,
  `mois_nom` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `mois`
--

INSERT INTO `mois` (`mois_id`, `mois_nom`) VALUES
(1, 'janvier'),
(2, 'février'),
(3, 'mars'),
(4, 'avril'),
(5, 'mai'),
(6, 'juin'),
(7, 'juillet'),
(8, 'août'),
(9, 'september'),
(10, 'octobre'),
(11, 'novembre'),
(12, 'décembre');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `email` varchar(30) NOT NULL,
  `mot_de_passe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `email`, `mot_de_passe`) VALUES
(1, 'paulette@biokop.fr', '$argon2i$v=19$m=65536,t=4,p=1$ZXJHdXF4L1FBNFBhVDBhVA$TNAoYl4Y/tiC2awLLygEwkxvADwX33nReX0zYb3JhrI');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`element_id`),
  ADD UNIQUE KEY `nom` (`element_nom`);

--
-- Index pour la table `intermediaire`
--
ALTER TABLE `intermediaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_element` (`id_element`),
  ADD KEY `id_mois` (`id_mois`);

--
-- Index pour la table `mois`
--
ALTER TABLE `mois`
  ADD PRIMARY KEY (`mois_id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `elements`
--
ALTER TABLE `elements`
  MODIFY `element_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT pour la table `intermediaire`
--
ALTER TABLE `intermediaire`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT pour la table `mois`
--
ALTER TABLE `mois`
  MODIFY `mois_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `intermediaire`
--
ALTER TABLE `intermediaire`
  ADD CONSTRAINT `inter_element` FOREIGN KEY (`id_element`) REFERENCES `elements` (`element_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inter_mois` FOREIGN KEY (`id_mois`) REFERENCES `mois` (`mois_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
