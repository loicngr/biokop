import {updateElementMonths, uploadElementImage, getMonths, addElement, deleteElement, getElementsWithMonths, updateElementName} from './api.js';

let inEditor = false;
let elementsArray = [];
const editedElement = {
    id: null,
    save: {
        month: null,
        name: null,
    },
    edited: {
        month: null,
        name: null,
        image: null,
    },

    clear() {
        this.id = null;
        this.save.name = null;
        this.save.month = null;
        this.edited.name = null;
        this.edited.month = null;
        this.edited.image = null;
    }
};

let MONTHS = [];

/**
 * Quand l'utilisateur clique sur le bouton "Editer"
 * @param {MouseEvent} evt
 */
function onClickEditElement(evt) {
    if (!inEditor) {
        inEditor = true;

        const baseRef = evt.target;
        let articleElement = baseRef.parentElement.parentElement;

        // On récupère dans le dom l'article via le parent de l'élément
        if (articleElement.nodeName.toLowerCase() !== 'article') {
            articleElement = articleElement.parentElement;
        }

        // On récupère l'id de l'élément de la bdd
        const {id} = articleElement?.dataset;
        const elementId = parseInt(id);
        const checkOcc = elementsArray.findIndex(el => el.id === elementId);
        if (checkOcc === -1) throw Error("impossible de trouver l'élement.");

        editedElement.id = elementId;
        editedElement.save.name = elementsArray[checkOcc].nom;
        editedElement.save.month = elementsArray[checkOcc].mois;

        articleElement.setAttribute('data-type', 'edited');
    }
}

/**
 * Quand l'utilisateur clique sur le bouton "Supprimer"
 * @param {MouseEvent} evt
 */
function onClickDeleteElement(evt) {
    const baseRef = evt.target;
    let articleElement = baseRef.parentElement.parentElement;

    // On récupère dans le dom l'article via le parent de l'élément
    if (articleElement.nodeName.toLowerCase() !== 'article') {
        articleElement = articleElement.parentElement;
    }

    // On récupère l'id de l'élément de la bdd
    const {id} = articleElement?.dataset;
    const elementId = parseInt(id);
    const checkOcc = elementsArray.findIndex(el => el.id === elementId);
    if (checkOcc === -1) throw Error("impossible de trouver l'élement.");

    deleteElement(id).then(r => {
        if (r?.status) {
            elementsArray.splice(checkOcc, 1);
            buildElements();
        } else {
            alert(r.data);
        }
    });

}

/**
 * Quand l'utilisateur clique sur le bouton "Fermer"
 * @param {MouseEvent} evt
 */
function onClickCloseElement(evt) {
    const baseRef = evt.target;
    let articleElement = baseRef.parentElement.parentElement;

    // On récupère dans le dom l'article via le parent de l'élément
    if (articleElement.nodeName.toLowerCase() !== 'article') {
        articleElement = articleElement.parentElement;
    }

    if (editedElement.id === 'new') {
        articleElement.remove();
    }

    articleElement.removeAttribute('data-type');

    inEditor = false;
    editedElement.clear();
}

function compareArray(arr1, arr2) {
    // Si les deux tableau ne sont pas de même taille
    if (arr1?.length !== arr2?.length) {
        return true;
    }

    const arr1Length = arr1.length;
    for (let i = 0; i < arr1Length; i++) {
        if (arr1[i] !== arr2[i]) return true;
    }
    return false;
}

function nameToId(arr) {
    if (!arr || arr.length === 0) return false;
    return arr?.map(name => {
        const month = MONTHS.find(el => el.mois_nom == name);
        if (month?.mois_id) {
            return parseInt(month.mois_id);
        }
    });
}

/**
 * Quand l'utilisateur clique sur le bouton "Valider"
 * @param {MouseEvent} evt
 */
function onClickValidElement(evt) {
    const baseRef = evt.target;
    let articleElement = baseRef.parentElement.parentElement;

    // On récupère dans le dom l'article via le parent de l'élément
    if (articleElement.nodeName.toLowerCase() !== 'article') {
        articleElement = articleElement.parentElement;
    }

    const {value} = articleElement.children[0].children[1];
    if (value.trim() !== editedElement.save.name) {
        editedElement.edited.name = value.trim();
    }

    // Si c'est un nouveau élément
    if (editedElement.id === 'new') {
        if (editedElement.edited.name && editedElement.edited.month?.length !== 0 && editedElement.edited.image) {
            const monthsId = nameToId(editedElement.edited.month);
            if (monthsId) {
                addElement(monthsId, editedElement.edited.name, editedElement.edited.image)
                    .then(r => {
                        if (r?.status) {
                            editedElement.clear();
                            articleElement.removeAttribute('data-type');
                            main();
                        } else {
                            alert(r.data);
                        }
                    });
            }
        }
        return;
    }

    if (editedElement.edited.name) {
        updateElementName(editedElement.edited.name, editedElement.id).then(r => {
            if (r?.status) {
                articleElement.children[0].children[0].textContent = editedElement.edited.name;
                alert('Nom bien modifié.');
            } else {
                alert(r.data);
            }
        })
    }

    if (editedElement.edited.month) {
        const monthsId = nameToId(editedElement.edited.month);
        if (monthsId && monthsId?.length !== 0) {
            updateElementMonths(monthsId, editedElement.id).then(r => {
                if (r?.status) {
                    alert('Mois bien modifié.');
                    main(false);
                } else {
                    alert(r.data);
                }
            });
        }
    }

    if (editedElement.edited.image) {
        uploadElementImage(editedElement.edited.image, editedElement.id).then(r => {
            if (r?.status) {
                alert('Image bien modifié.');
            } else {
                alert(r.data);
            }
        });
    }

    articleElement.removeAttribute('data-type');
    inEditor = false;
    editedElement.clear();
}

/**
 * Quand l'utilisateur clique sur le bouton "Image"
 * @param {MouseEvent} evt
 */
function onClickImageElement(evt) {
    const {firstElementChild} = evt.target.parentElement;

    firstElementChild.click();
    firstElementChild.addEventListener('change', uploadImage);
}
function uploadImage(evt) {
    const {files} = evt.target;
    editedElement.edited.image = files[0];
}

/**
 * Quand l'utilisateur clique sur le bouton "Calendrier"
 * @param {MouseEvent} evt
 */
function onClickCalendarElement(evt) {
    buildSelectionMonths();
}

/**
 * Ajouter un nouveau fruit ou légume
 */
function makeNewElement() {
    const parentElement = document.querySelector("#container section");

    const articleElement = document.createElement("article");
    articleElement.setAttribute("data-id", 'new');

    // Title
    const h3Element = document.createElement("h3");
    const spanElement = document.createElement("span");
    const inputElement = document.createElement("input");

    h3Element.appendChild(spanElement);
    h3Element.appendChild(inputElement);

    inputElement.type = 'text';
    inputElement.placeholder = 'Nom du légume ou Fruit';
    editedElement.id = 'new';

    // Boutons
    const divElement = document.createElement("div");
    divElement.classList.add("buttons");

    // Construction des 6 boutons
    for (let i = 0; i <= 3; i++) {
        const buttonElement = document.createElement("button");
        const imageElement = document.createElement("img");

        switch (i) {
            case 0:
                imageElement.src = './assets/img/calendar.svg';
                imageElement.alt = 'calendrier';
                buttonElement.setAttribute("data-show", "edited");
                buttonElement.addEventListener('click', onClickCalendarElement);
                break;
            case 1:
                const inputElement = document.createElement('input');
                inputElement.type = 'file';
                inputElement.accept = 'image/*';
                inputElement.style.display = 'none';
                imageElement.src = './assets/img/image.svg';
                imageElement.alt = 'image';
                buttonElement.setAttribute("data-show", "edited");
                buttonElement.addEventListener('click', onClickImageElement);
                buttonElement.appendChild(inputElement);
                break;
            case 2:
                imageElement.src = './assets/img/valid.svg';
                imageElement.alt = 'valider';
                buttonElement.setAttribute("data-show", "edited");
                buttonElement.addEventListener('click', onClickValidElement);
                break;
            case 3:
                imageElement.src = './assets/img/close.svg';
                imageElement.alt = 'fermer';
                buttonElement.setAttribute("data-show", "edited");
                buttonElement.addEventListener('click', onClickCloseElement);
                break;
            default:
                break;
        }

        buttonElement.appendChild(imageElement);
        divElement.appendChild(buttonElement);
    }

    articleElement.appendChild(h3Element);
    articleElement.appendChild(divElement);
    articleElement.setAttribute('data-type', 'edited');
    if (parentElement.childElementCount) parentElement.prepend(articleElement);
    else parentElement.appendChild(articleElement);
}

/**
 * Construction des éléments dans le HTML avec les écouteurs d'événements
 * */
function buildElements() {
    const parentElement = document.querySelector("#container section");
    parentElement.innerHTML = "";
    elementsArray.forEach((element) => {

        const articleElement = document.createElement("article");
        articleElement.setAttribute("data-id", element.id);

        // Title
        const h3Element = document.createElement("h3");
        const spanElement = document.createElement("span");
        const inputElement = document.createElement("input");

        h3Element.appendChild(spanElement);
        h3Element.appendChild(inputElement);

        inputElement.type = 'text';
        inputElement.value = element.nom;
        spanElement.innerText = element.nom;

        // Boutons
        const divElement = document.createElement("div");
        divElement.classList.add("buttons");

        // Construction des 6 boutons
        for (let i = 0; i <= 5; i++) {
            const buttonElement = document.createElement("button");
            const imageElement = document.createElement("img");

            switch (i) {
                case 0:
                    imageElement.src = './assets/img/calendar.svg';
                    imageElement.alt = 'calendrier';
                    buttonElement.setAttribute("data-show", "edited");
                    buttonElement.addEventListener('click', onClickCalendarElement);
                    break;
                case 1:
                    const inputElement = document.createElement('input');
                    inputElement.type = 'file';
                    inputElement.accept = 'image/*';
                    inputElement.style.display = 'none';
                    imageElement.src = './assets/img/image.svg';
                    imageElement.alt = 'image';
                    buttonElement.setAttribute("data-show", "edited");
                    buttonElement.addEventListener('click', onClickImageElement);
                    buttonElement.appendChild(inputElement);
                    break;
                case 2:
                    imageElement.src = './assets/img/valid.svg';
                    imageElement.alt = 'valider';
                    buttonElement.setAttribute("data-show", "edited");
                    buttonElement.addEventListener('click', onClickValidElement);
                    break;
                case 3:
                    imageElement.src = './assets/img/close.svg';
                    imageElement.alt = 'fermer';
                    buttonElement.setAttribute("data-show", "edited");
                    buttonElement.addEventListener('click', onClickCloseElement);
                    break;
                case 4:
                    imageElement.src = './assets/img/edit.svg';
                    imageElement.alt = 'editer';
                    buttonElement.addEventListener('click', onClickEditElement);
                    break;
                case 5:
                    imageElement.src = './assets/img/delete.svg';
                    imageElement.alt = 'supprimer';
                    buttonElement.addEventListener('click', onClickDeleteElement);
                    break;
                default:
                    break;
            }

            buttonElement.appendChild(imageElement);
            divElement.appendChild(buttonElement);
        }

        articleElement.appendChild(h3Element);
        articleElement.appendChild(divElement);
        parentElement.appendChild(articleElement);
    });
}


/**
 * Cache le modal pour la sélection des mois
 */
function closeSelectionMonth() {
    const parentElement = document.getElementById("selectionMonths");
    parentElement.setAttribute('data-show', 'false');
    editedElement.edited.month = [];

    // On boucle dans tous nos boutons avec les mois
    [...parentElement.children[0].children].forEach(el => {
        // Si le bouton est sélectionné
        if (el?.dataset?.selected) {
            // on ajoute le mois du bouton dans le tableau d'édition des mois
            if (el.dataset.selected === 'true') editedElement.edited.month.push(el.textContent);
        }
    });
    // Si le tableau est vide, on le défini a null
    if (editedElement.edited.month?.length === 0) editedElement.edited.month = null;
    // Si le tableau n'a pas changé, on le défini à null
    if (editedElement.edited.month && editedElement.save.month ) {
        if (!compareArray(editedElement.edited.month, editedElement.save.month))  editedElement.edited.month = null;
    }
}

/**
 * Quand on clique sur le bouton d'un mois
 * @param {MouseEvent} evt
 */
function onClickBtnSelectionMonth(evt) {
    const {selected} = evt?.target?.dataset;

    if (selected === 'true') {
        evt.target.setAttribute('data-selected', 'false');
    } else {
        evt.target.setAttribute('data-selected', 'true');
    }
}

/**
 * Affiche le modal pour la sélection des mois
 */
function showSelectionMonth() {
    const parentElement = document.getElementById("selectionMonths");
    parentElement.setAttribute('data-show', 'true');
}

/**
 * Construit tous les boutons des mois on fonction des mois enregistrés en bdd
 */
function buildSelectionMonths() {
    getMonths().then(r => {
       if (r?.status) {
           const parentElement = document.getElementById("selectionMonths");
           parentElement.innerHTML = "";
           const divElement = document.createElement("div");
           divElement.classList.add('buttons');

           parentElement.appendChild(divElement);
           r.data.forEach((month) => {
               if (!month?.hasOwnProperty('mois_nom') || !month?.hasOwnProperty('mois_id')) return;

               const buttonElement = document.createElement("button");
               buttonElement.innerText = month.mois_nom;
               buttonElement.setAttribute('data-id', month.mois_id);

               if (editedElement.id !== 'new') {
                   const indexMonths = editedElement.save.month.indexOf(month.mois_nom);
                   buttonElement.setAttribute('data-selected', "false");

                   if (indexMonths !== -1) {
                       buttonElement.setAttribute('data-selected', "true");
                   }
               } else {
                   if (editedElement.edited.month) {
                        const indexMonths = editedElement.edited.month.indexOf(month.mois_nom);
                        if (indexMonths !== -1) {
                            buttonElement.setAttribute('data-selected', "true");
                        } else {
                            buttonElement.setAttribute('data-selected', "false");
                        }
                   } else {
                       buttonElement.setAttribute('data-selected', "false");
                   }
               }

               buttonElement.addEventListener('click', onClickBtnSelectionMonth);

               divElement.appendChild(buttonElement);
           })
           const buttonElement = document.createElement("button");
           buttonElement.innerText = "Fermer";
           buttonElement.addEventListener('click', closeSelectionMonth);
           divElement.appendChild(buttonElement);

           showSelectionMonth();
       }
    });
}

/**
 * Fonction principal qui récupère tous les éléments avec leurs mois
 *
 * @param {boolean} build - Construction/ReConstruction du DOM dans la page HTML
 */
function main(build = true) {
    /**
     * On récupère tous les éléments avec leurs mois
     * Puis on supprime les occurrences pour n'avoir qu'un seul fruit identique avec tous ces mois.
     * */
    getElementsWithMonths().then(r => {
        if (r?.status && r?.data) {
            elementsArray = [];
            r.data.forEach((element) => {
                // Contient l'index du fruit dans le tableau si il existe déjà
                const checkOcc = elementsArray.findIndex(el => el.nom === element.element_nom);

                if (checkOcc === -1) {
                    const obj = {id: parseInt(element.element_id), nom: element.element_nom, mois: []};
                    obj.mois.push(element.mois_nom);
                    elementsArray.push(obj);
                } else {
                    elementsArray[checkOcc].mois.push(element.mois_nom);
                }
            });
            if (build) buildElements();
        } else {
            alert('Impossible de récupérer les éléments.');
        }
    });
}

main();
getMonths().then(r => {
    MONTHS = r.data;
});
document.getElementById("addElementBtn").addEventListener('click', makeNewElement);