import {request} from "./request.js";

/**
 * Récupérer les informations d'un Mois par son ID
 *
 * @return {Promise<{data: null, status: boolean}>}
 */
async function getMonthsBtId() {
    return await request("getMonths&id=5");
}

/**
 * Récupérer tous les fruits et légumes
 * @return {Promise<{data: null, status: boolean}>}
 */
async function getElements() {
    return await request("getElements");
}

async function getElementsWithMonths() {
    return await request("getElementsMonths");
}

/**
 * Récupérer tous les mois
 *
 * @return {Promise<{data: null, status: boolean}>}
 */
async function getMonths() {
    return await request("getMonths");
}

/**
 * Récupérer tous les fruits et légumes en fonction de l'ID du Mois
 *
 * @return {Promise<{data: null, status: boolean}>}
 */
async function getElementByMonthId() {
    return await request("getElement&month_id=4");
}

/**
 * Récupérer tous les Mois d'un fruit ou légume
 * @return {Promise<void>}
 */
async function getElementMonths() {
    const response = await request("getElement&id=14");
    console.log(response);
}

/**
 * Ajouter une fruit ou légume
 *
 * @param {array} mois
 * @param {string} nom
 * @param {Blob} blob
 * @return {Promise<{data: null|string, status: boolean}>}
 */
async function addElement(mois, nom, blob) {
    if (!blob?.name) return {status: false, data: "Image invalide."};

    const response = await request("addElement", "post", {
        form: {
            nom: nom,
            image: blob.name,
            mois: mois
        }
    });
    if (response?.status) await uploadImage(blob);
    return response;
}

/**
 * Mise à jour du nom d'un élément
 *
 * @param {string} name
 * @param {number} id
 * @return {Promise<{data: null, status: boolean}>}
 */
async function updateElementName(name, id) {
    return await request("updateElementName", "post", {
        form: {
            id: id,
            nom: name
        }
    });
}

/**
 * Téléversement d'une image dans un élément
 *
 * @param {Blob} blob
 * @param {number} id
 * @return {Promise<{data: null, status: boolean}>}
 */
async function uploadElementImage(blob, id) {
    return await request("updateElementImage", "post", {
        form: {
            id: id,
            file: blob
        }
    });
}

/**
 * Téléversement d'une image
 *
 * @param {Blob} blob
 * @return {Promise<{data: null, status: boolean}>}
 */
async function uploadImage(blob) {
    return await request("uploadImage", "post", {
        form: {
            file: blob
        }
    });
}

/**
 * Mise à jour des mois d'un élément
 *
 * @param {array} months
 * @param {number} id
 * @returns {Promise<{data: null, status: boolean}>}
 */
async function updateElementMonths(months, id) {
    return await request("updateElementMonths", "post", {
        form: {
            id: id,
            mois: months
        }
    });
}

/**
 * Supprimer un fruit ou légume
 *
 * @param {number} id
 * @return {Promise<{data: null, status: boolean}>}
 */
async function deleteElement(id) {
    return await request(`deleteElement&id=${id}`, "delete");
}

/**
 * Connexion de l'utilisateur
 *
 * @param {string} email
 * @param {string} password
 * @returns {Promise<{data: null, status: boolean}>}
 */
async function loginUser(email, password) {
    return await request("connectUser", "post", {
        form: {
            email: email,
            password: password
        }
    });
}

/**
 * Vérification si l'utilisateur est connecté?
 *
 * @returns {Promise<{data: null, status: boolean}>}
 */
async function checkIsLoggedIn() {
    return await request("isLoggedIn");
}

export {updateElementMonths, uploadElementImage, addElement, deleteElement, getElements, getMonths, loginUser, checkIsLoggedIn, getElementsWithMonths, updateElementName};