<?php
/**
 * Bdd
 *
 * Connexion à une base de données avec PDO
 */
class Bdd
{
    protected $mysql_config;
    protected $pdo;

    function __construct() {
        require_once ".env.php";

        if (isset($MYSQL_LOGIN) && isset($MYSQL_PASS) && isset($MYSQL_DBNAME) && isset($MYSQL_HOST)):
            $this->mysql_config = array(
                "login" => $MYSQL_LOGIN,
                "pass" => $MYSQL_PASS,
                "db" => $MYSQL_DBNAME,
                "host" => $MYSQL_HOST
            );
            $this->connection();
            else:
                echo json_encode(["error" => "Fichier .env.php non trouvé."]);
        endif;
    }
    /**
     * Connexion base de données
     */
    private function connection()
    {
        try {
            $this->pdo = new PDO(
                "mysql:host={$this->mysql_config['host']};dbname={$this->mysql_config['db']};charset=utf8mb4",
                $this->mysql_config["login"],
                $this->mysql_config["pass"]
            );
            /**
                Affichage des erreurs PDO
             */
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo json_encode(["error" => $e->getMessage()]);
        }
    }

    /**
     * Retourne les mois enregistrés en Base de données
     *
     * @return false|string
     */
    public function getMonths()
    {
        $request = $this->pdo->prepare("SELECT mois_id, mois_nom FROM mois");
        try
        {
            $request->execute();
        } catch (PDOException $exception)
        {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        $result = $request->fetchAll(PDO::FETCH_OBJ);
        return json_encode($result);
    }

    /**
     * Retourne les mois enregistrés en Base de données
     *
     * @param $id
     * @return false|string
     */
    public function getMonthByID($id)
    {
        if (isset($id) && !empty($id) && is_int($id)):
            $request = $this->pdo->prepare("SELECT mois_id, mois_nom FROM mois WHERE mois_id = :id");
            $request->bindParam(":id", $id);
            try
            {
                $request->execute();
            } catch (PDOException $exception)
            {
                return json_encode(["error" => "Erreur lors de la requete."]);
            }
            $result = $request->fetch(PDO::FETCH_OBJ);
            return json_encode($result);
            else:
                return json_encode(["error" => "ID mal formé."]);
        endif;
    }

    /**
     * Retourne les éléments enregistrés en Base de données
     *
     * @return false|string
     */
    public function getElements()
    {
        $request = $this->pdo->prepare("SELECT element_id, element_nom, element_image, mois_nom, mois_id FROM intermediaire 
            INNER JOIN elements ON intermediaire.id_element = element_id 
            INNER JOIN mois ON intermediaire.id_mois = mois_id");
        try
        {
            $request->execute();
        } catch (PDOException $exception)
        {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        $result = $request->fetchAll(PDO::FETCH_OBJ);
        return json_encode($result);
    }
    /**
     * Retourne les éléments enregistrés en Base de données avec leurs mois
     *
     * @return false|string
     */
    public function getElementsMonths()
    {
        $request = $this->pdo->prepare("SELECT element_id, element_nom, mois_nom FROM intermediaire 
            INNER JOIN elements ON intermediaire.id_element = element_id    
            INNER JOIN mois ON intermediaire.id_mois = mois_id");
        try
        {
            $request->execute();
        } catch (PDOException $exception)
        {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        $result = $request->fetchAll(PDO::FETCH_OBJ);
        return json_encode($result);
    }

    /**
     * Retourne les éléments via le mois dans la base de données
     *
     * @param $month_id
     * @return false|string
     */
    public function getElementByMonth($month_id)
    {
        if (isset($month_id) && !empty($month_id) && is_int($month_id)):
            $request = $this->pdo->prepare("SELECT element_id, element_nom, element_image FROM intermediaire 
                INNER JOIN elements ON intermediaire.id_element = element_id 
                WHERE intermediaire.id_mois = :id");
            $request->bindParam(":id", $month_id);
            try
            {
                $request->execute();
            } catch (PDOException $exception)
            {
                return json_encode(["error" => "Erreur lors de la requete."]);
            }
            $result = $request->fetchAll(PDO::FETCH_OBJ);
            return json_encode($result);
        else:
            return json_encode(["error" => "ID mal formé."]);
        endif;
    }

    public function getElementMonths($element_id)
    {
        $request = $this->pdo->prepare("SELECT mois_nom, mois_id FROM intermediaire 
                INNER JOIN mois ON intermediaire.id_mois = mois_id 
                WHERE intermediaire.id_element = :id");
        $request->bindParam(":id", $element_id);
        try
        {
            $request->execute();
        } catch (PDOException $exception)
        {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        $result = $request->fetchAll(PDO::FETCH_OBJ);
        return json_encode($result);
    }

    /**
     * Retourne les éléments via le nom dans la base de données
     *
     * @param $name
     * @return false|string
     */
    public function getElementByName($name)
    {
        if (isset($name) && !empty($name) && !is_int($name)):
            $request = $this->pdo->prepare("SELECT element_id, element_nom, element_image FROM elements WHERE element_nom = :name");
            $request->bindParam(":name", $name);
            try
            {
                $request->execute();
            } catch (PDOException $exception)
            {
                return json_encode(["error" => "Erreur lors de la requete."]);
            }
            $result = $request->fetchAll(PDO::FETCH_OBJ);
            return json_encode($result);
        else:
            return json_encode(["error" => "Nom mal formé."]);
        endif;
    }


    public function postElement($element_data)
    {
        $months = explode(',', $element_data['months']);

        $request = $this->pdo->prepare("INSERT INTO elements (element_nom, element_image) VALUES (:name, :image)");
        $request->bindParam(":name", $element_data['name']);
        $request->bindParam(":image", $element_data['image']);

        try {
            $request->execute();
        } catch (PDOException $exception) {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }

        // On récupère l'id de l'élement
        $request = $this->pdo->prepare("SELECT element_id FROM elements WHERE element_nom = :name");
        $request->bindParam(":name", $element_data['name']);
        try {
            $request->execute();
        } catch (PDOException $exception) {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        $result = $request->fetch(PDO::FETCH_OBJ);
        $elementId = intval($result->element_id);

        // On ajoute tous les mois du fruits
        foreach ($months as $month_id):
            $request = $this->pdo->prepare("INSERT INTO intermediaire (id_element, id_mois) VALUES (:element_id, :month_id)");
            $request->bindParam(":element_id", $elementId);
            $request->bindParam(":month_id", intval($month_id));
            try
            {
                $request->execute();
            } catch (PDOException $exception)
            {
                return json_encode(["error" => "Erreur lors de la requete."]);
            }
        endforeach;

        return true;
    }

    public function updateElementName($element_data)
    {
        $request = $this->pdo->prepare("UPDATE elements SET element_nom = :name WHERE element_id = :id");
        $request->bindParam(":name", $element_data['name']);
        $request->bindParam(":id", $element_data['id']);

        try
        {
            $verify = $request->execute();
        } catch (PDOException $exception)
        {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        return $verify;
    }

    public function updateElementMonths($element_data)
    {
        // On commence par supprimé tous les mois de l'élément
        $request = $this->pdo->prepare("DELETE FROM intermediaire WHERE id_element = :id");
        $request->bindParam(":id", $element_data['id']);
        try {
            $request->execute();
        } catch (PDOException $exception) {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }

        // On ajoute ensuite les nouveaux mois
        $months = explode(',', $element_data['months']);
        foreach ($months as $month_id):
            $request = $this->pdo->prepare("INSERT INTO intermediaire (id_element, id_mois) VALUES (:element_id, :month_id)");
            $request->bindParam(":element_id", $element_data['id']);
            $request->bindParam(":month_id", intval($month_id));
            try {
                $request->execute();
            } catch (PDOException $exception) {
                return json_encode(["error" => "Erreur lors de la requete."]);
            }
        endforeach;

        return true;
    }

    /**
     * Retourne le nom d'un fruit ou légume
     * @param $id
     * @return mixed
     */
    public function getElementImageName($id)
    {
        $request = $this->pdo->prepare("SELECT element_image FROM elements WHERE element_id = :id");
        $request->bindParam(":id", $id);
        $request->execute();
        $result = $request->fetch(PDO::FETCH_OBJ);
        return $result->element_image;
    }
    public function deleteLocalImage($name)
    {
        $path = explode('/', $_SERVER['DOCUMENT_ROOT']);
        $path = implode('/', $path);
        unlink("$path/uploads/$name");
    }

    public function updateElementImage($element_data)
    {
        // On récupère l'ancienne image
        $old_image = $this->getElementImageName($element_data['id']);

        // Suppression de l'ancienne image en local
        $this->deleteLocalImage($old_image);

        // Mise à jour dans la bbd du nouveau nom
        $request = $this->pdo->prepare("UPDATE elements SET element_image = :name WHERE element_id = :id");
        $request->bindParam(":name", $element_data['name']);
        $request->bindParam(":id", $element_data['id']);

        try {
            $verify = $request->execute();
        } catch (PDOException $exception) {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        return $verify;
    }

    public function connectUser($user_data)
    {
        $email = $user_data['email'];
        $password = $user_data['password'];

        $request = $this->pdo->prepare("SELECT email, mot_de_passe FROM utilisateurs WHERE email = :email");
        $request->bindParam(":email", $email);
        $request->execute();
        $result = $request->fetch(PDO::FETCH_OBJ);
        if (!empty($result)):
            $hash = $result->mot_de_passe;

            $verify = password_verify($password, $hash);
            if ($verify) {
                if (!isset($_SESSION)) session_start();
                $_SESSION["loggedIn"] = true;
                $_SESSION["email"] = $email;
            }
            return $verify;
        else:
            return false;
        endif;
    }

    public function deleteElementById($element_id)
    {
        $old_image = $this->getElementImageName($element_id);
        $this->deleteLocalImage($old_image);

        $request = $this->pdo->prepare("DELETE FROM elements WHERE element_id = :id");
        $request->bindParam(":id", $element_id);
        try
        {
            $verify = $request->execute();
        } catch (PDOException $exception)
        {
            return json_encode(["error" => "Erreur lors de la requete."]);
        }
        return $verify;
    }
}
