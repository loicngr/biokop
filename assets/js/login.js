import {loginUser, checkIsLoggedIn} from './api.js';

/**
 * Soumission du formulaire de connexion
 *
 * @param {Event} evt
 * @return {Promise<void>}
 */
function submitForm(evt) {
    evt.preventDefault();

    const {form} = evt.target;
    const emailValue = form[0]?.value?.trim();
    const passwordValue = form[1]?.value?.trim();

    if (emailValue && passwordValue) {
        loginUser(emailValue, passwordValue).then(r => {
            if (r?.status) {
                if (r?.data) {
                    location.href = '/admin.php';
                    return;
                }
            }
            alert('Non valide!');
        });
    }
}

checkIsLoggedIn().then(r => {
    if (r?.status && r?.data) {
        location.href = '/admin.php';
    }
})
document.querySelector("form button[type='submit']").addEventListener('click', submitForm);