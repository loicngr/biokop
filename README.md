# Paramétrage :

## Fichier d'environement : 
- Création du fichier `.env.php` dans le dossier `models`.
- Insérer les variables suivantes dans `.env.php` : 
```php
<?php
     $MYSQL_LOGIN = "";
     $MYSQL_PASS = "";
     $MYSQL_DBNAME = "";
     $MYSQL_HOST = "localhost";
```

## Démarrer le serveur :
- > `php -S localhost`
  >
## Adresse du site
- https://loicngr.alwaysdata.net

## Page de connexion
- https://loicngr.alwaysdata.net/login.html